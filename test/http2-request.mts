/**
 * SPDX-PackageName: kwaeri/http2-request
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import * as fs from 'fs/promises';
import * as _path from 'path';
import { Filesystem } from '@kwaeri/filesystem';
import { Payload, Http2Request, ClientHttp2Promise  } from '../src/http2-request.mjs';


// DEFINES
//let http2Request = new Http2Request();

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {


        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );


    }
);


describe(
    'Payload Feature Test Suite',
    () => {


        describe(
            'Payload toString() test [I - With Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload( `{ "propA": "First", "propB": "second", "propC": "third" }` ),
                            control = { "propA": "First", "propB": "second", "propC": "third" };

                        //console.log( example.toString() );
                        return Promise.resolve(
                        assert.equal( example.toString(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );

        describe(
            'Payload toString() test [II - With No Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload(),
                            control = "";

                        //console.log( example.toString() );
                        return Promise.resolve(
                        assert.equal( example.toString(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );

        describe(
            'Payload toJSON() test [III - With Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload( `{ "propA": "First", "propB": "second", "propC": "third" }` ),
                            control = { "propA": "First", "propB": "second", "propC": "third" };

                        //console.log( example.toJSON() );
                        return Promise.resolve(
                        assert.equal( example.toJSON(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );

        describe(
            'Payload toJSON() test [IV - With No Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload(),
                            control = "";

                        //console.log( example.toJSON() );
                        return Promise.resolve(
                        assert.equal( example.toJSON(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );

        describe(
            'Payload parse() test [V - With Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload( ),
                            control = { "propA": "First", "propB": "second", "propC": "third" };

                        example.parse( `{ "propA": "First", "propB": "second", "propC": "third" }` );

                        //console.log( example.toJSON() );
                        return Promise.resolve(
                        assert.equal( example.toJSON(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );

        describe(
            'Payload parse() test [V - With No Value]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const example = new Payload( ),
                            control = "";

                        example.parse( `` );

                        //console.log( example.toJSON() );
                        return Promise.resolve(
                        assert.equal( example.toJSON(), JSON.stringify( control ) )
                        );
                    }
                );
            }
        );


    }
);


describe(
    'Http2-Request Feature Test Suite',
    () => {


        describe(
            'http2-request makeRequest() test [I - READ ONLY]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const path              = Filesystem.getPathToCWD(),
                              host              = 'https://gitlab.com',
                              prefix            = '/api/v4/projects',
                              projectPath       = _path.join( prefix, "20989565" ),
                              baseTreePath      = _path.join( projectPath, 'repository/tree'),
                              baseTreeQuery     = baseTreePath + '?recursive=true',
                              baseFilesQuery    = _path.join( projectPath, 'repository/files' ),
                              context           = this;


                        const tree = ( await Http2Request.makeRequest( host, baseTreeQuery, false ) as any ).payload;

                        //console.log( tree );

                        const control = [{"id":"af9be8e06619f6c92b7a977d229df1335cda43fc","name":"DEP5","type":"tree","path":"DEP5","mode":"040000"},{"id":"5e5294c50c85984e2d7c7504336a5a3aa3d6d16e","name":"LICENSES","type":"tree","path":"LICENSES","mode":"040000"},{"id":"6dea5f1512234ce4bed41862941c6c0039344b7d","name":"src","type":"tree","path":"src","mode":"040000"},{"id":"497054f3fe91411cad2d7accc3a41e9ca37c969e","name":"assets","type":"tree","path":"src/assets","mode":"040000"},{"id":"16c4d63a9dbf6cb3ca013f24841c5e658ffdfe85","name":"test","type":"tree","path":"test","mode":"040000"},{"id":"cde1b8d0a80d5349ea3bbb3764f7e6b70c05cc6c","name":".gitignore","type":"blob","path":".gitignore","mode":"100644"},{"id":"a962941e1624c69306e5bd88c4064cd693578d75","name":".gitlab-ci.yml","type":"blob","path":".gitlab-ci.yml","mode":"100644"},{"id":"38aa3d308e345c5331c00e7c8c2f1318f86f3bbd","name":".mocharc.json","type":"blob","path":".mocharc.json","mode":"100644"},{"id":"b3709c0e5bf24f45cfe2815d37750faea04415ba","name":".npmignore","type":"blob","path":".npmignore","mode":"100644"},{"id":"f3e7bfb900c88f88d78baf7dc9399720747145f6","name":".nycrc","type":"blob","path":".nycrc","mode":"100644"},{"id":"f997f161c1e0f8f5f6bb55d11b6fccb3a1defe40","name":".xfmrc","type":"blob","path":".xfmrc","mode":"100644"},{"id":"45718210d00765948c881eaef84e48c087f08e42","name":"AUTHORS.md","type":"blob","path":"AUTHORS.md","mode":"100644"},{"id":"609d29fe8a8e1ec893ce91081da885f55187f86d","name":"copyright","type":"blob","path":"DEP5/copyright","mode":"100644"},{"id":"33b574d91d5f8e0c9bc1d198ecd52b255645adc1","name":"LICENSE.md","type":"blob","path":"LICENSE.md","mode":"100644"},{"id":"efd7bc14aec771e4c8e73f218b5b8f6170373e2c","name":"Apache-2.0-WITH-LLVM-exception.txt","type":"blob","path":"LICENSES/Apache-2.0-WITH-LLVM-exception.txt","mode":"100644"},{"id":"09691ac043472786e5b3c2429b4597e6b1420d4a","name":"Apache-2.0.txt","type":"blob","path":"LICENSES/Apache-2.0.txt","mode":"100644"},{"id":"653792b7c1fd05263cd83870f7f2814067844989","name":"LLVM-exception.txt","type":"blob","path":"LICENSES/LLVM-exception.txt","mode":"100644"},{"id":"0e6178bb942b6a83b17a3ce8968da5674c147ace","name":"MIT.txt","type":"blob","path":"LICENSES/MIT.txt","mode":"100644"},{"id":"082b15d56982994a8b47aed85134967e392bdaa6","name":"README.md","type":"blob","path":"README.md","mode":"100644"},{"id":"d1902015588867f826639b66cbf16854e3bf9469","name":"index.mts","type":"blob","path":"index.mts","mode":"100644"}];

                        //console.log( example.toJSON() );
                        return Promise.resolve(
                        assert.equal( tree[0].id/*JSON.stringify( tree )*/, control[0].id/*JSON.stringify( control )*/ )
                        );
                    }
                );
            }
        );

        describe(
            'http2-request makeRequest() test [II - READ & WRITE]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        const cwd              = Filesystem.getPathToCWD(),
                              host              = 'https://gitlab.com',
                              prefix            = '/api/v4/projects',
                              projectPath       = _path.join( prefix, "20989565" ),
                              baseTreePath      = _path.join( projectPath, 'repository/tree'),
                              baseTreeQuery     = baseTreePath + '?recursive=true',
                              baseFilesQuery    = _path.join( projectPath, 'repository/files' ),
                              context           = this;


                        const {
                            result,
                            path,
                            fullPath
                         } = ( await Http2Request.makeRequest( host, baseTreeQuery, 'test-payload.json' ) as ClientHttp2Promise ).fileSystemPromiseBits;


                        //console.log( tree );
                        let downloaded;

                        if( result )
                            downloaded = JSON.parse( await fs.readFile( fullPath!, { encoding: "utf8" } ) );

                        const control = [{"id":"af9be8e06619f6c92b7a977d229df1335cda43fc","name":"DEP5","type":"tree","path":"DEP5","mode":"040000"},{"id":"5e5294c50c85984e2d7c7504336a5a3aa3d6d16e","name":"LICENSES","type":"tree","path":"LICENSES","mode":"040000"},{"id":"6dea5f1512234ce4bed41862941c6c0039344b7d","name":"src","type":"tree","path":"src","mode":"040000"},{"id":"497054f3fe91411cad2d7accc3a41e9ca37c969e","name":"assets","type":"tree","path":"src/assets","mode":"040000"},{"id":"16c4d63a9dbf6cb3ca013f24841c5e658ffdfe85","name":"test","type":"tree","path":"test","mode":"040000"},{"id":"cde1b8d0a80d5349ea3bbb3764f7e6b70c05cc6c","name":".gitignore","type":"blob","path":".gitignore","mode":"100644"},{"id":"a962941e1624c69306e5bd88c4064cd693578d75","name":".gitlab-ci.yml","type":"blob","path":".gitlab-ci.yml","mode":"100644"},{"id":"38aa3d308e345c5331c00e7c8c2f1318f86f3bbd","name":".mocharc.json","type":"blob","path":".mocharc.json","mode":"100644"},{"id":"b3709c0e5bf24f45cfe2815d37750faea04415ba","name":".npmignore","type":"blob","path":".npmignore","mode":"100644"},{"id":"f3e7bfb900c88f88d78baf7dc9399720747145f6","name":".nycrc","type":"blob","path":".nycrc","mode":"100644"},{"id":"f997f161c1e0f8f5f6bb55d11b6fccb3a1defe40","name":".xfmrc","type":"blob","path":".xfmrc","mode":"100644"},{"id":"45718210d00765948c881eaef84e48c087f08e42","name":"AUTHORS.md","type":"blob","path":"AUTHORS.md","mode":"100644"},{"id":"609d29fe8a8e1ec893ce91081da885f55187f86d","name":"copyright","type":"blob","path":"DEP5/copyright","mode":"100644"},{"id":"33b574d91d5f8e0c9bc1d198ecd52b255645adc1","name":"LICENSE.md","type":"blob","path":"LICENSE.md","mode":"100644"},{"id":"efd7bc14aec771e4c8e73f218b5b8f6170373e2c","name":"Apache-2.0-WITH-LLVM-exception.txt","type":"blob","path":"LICENSES/Apache-2.0-WITH-LLVM-exception.txt","mode":"100644"},{"id":"09691ac043472786e5b3c2429b4597e6b1420d4a","name":"Apache-2.0.txt","type":"blob","path":"LICENSES/Apache-2.0.txt","mode":"100644"},{"id":"653792b7c1fd05263cd83870f7f2814067844989","name":"LLVM-exception.txt","type":"blob","path":"LICENSES/LLVM-exception.txt","mode":"100644"},{"id":"0e6178bb942b6a83b17a3ce8968da5674c147ace","name":"MIT.txt","type":"blob","path":"LICENSES/MIT.txt","mode":"100644"},{"id":"082b15d56982994a8b47aed85134967e392bdaa6","name":"README.md","type":"blob","path":"README.md","mode":"100644"},{"id":"d1902015588867f826639b66cbf16854e3bf9469","name":"index.mts","type":"blob","path":"index.mts","mode":"100644"}];

                        const pfs = new Filesystem(),
                            created = await pfs.exists( _path.join( cwd, "test-payload.json" ) );

                        let deleted;

                        if( created )
                            deleted = await pfs.delete( _path.join( cwd, "test-payload.json" ) );
                            //if( deleted.result )

                        return Promise.resolve( assert.equal( downloaded[0].id/*JSON.stringify( downloaded )*/, control[0].id/*JSON.stringify( control )*/ ) );
                        //console.log( example.toJSON() );

                        //assert.equal( JSON.stringify( tree ), JSON.stringify( control ) );
                    }
                );
            }
        );


    }
);
