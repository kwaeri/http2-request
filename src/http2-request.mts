/**
 * SPDX-PackageName: kwaeri/http2-request
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as fs from 'fs';
import * as _path from 'path';
import * as http2 from 'http2';
import { Filesystem } from '@kwaeri/filesystem';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';

// DEFINES
const _ = new kdt();//,
    //_progress = new Progress();

/* Configure Debug module support */
const DEBUG = debug( 'kue:http2-request' );


export type ClientHttp2Promise = {
    fileSystemPromiseBits: {
        type?: string;
        result?: boolean;
        path?: string;
        fullPath?: string;
    };
    payload?: any;
};


/**
 * THe idea is that the 'string' that makes up the payload IS json.
 *
 * We want to be able to have it automatically converted into an object and accessible
 * and retain methods (built-in) to convert back and forth.
 */
export class Payload extends String {
    private meta; any = null;

    constructor( value?: string ) {
        super( value );

        if( value && _.type( value ) === "string" )
            this.meta = JSON.parse( value );
    }

    public toString(): string {
        return this.meta ? JSON.stringify( this.meta ) : JSON.stringify( "" );
    }

    public toJSON(): string {
        return this.meta ? JSON.stringify( this.meta ) : JSON.stringify( "" );
    }

    public parse( data?: string ): any {
        if( data && _.type( data ) === "string" )
            this.meta = JSON.parse( data );

        return this.meta ? this.meta : null;
    }
}


/**
 * EndpointGenerator
 *
 * Extends the { Filesystem } class, which implements the { BaseFilesystem }
 * Interface.
 *
 * THe { EndpointGenerator } facilitates the generating of file contents
 * for API Endpoints depending on the requirements of the end user.
 */
export abstract class Http2Request {
    /**
     * Method to make an API request to a remote client. It either takes a file path to write the
     * response to (as JSON), or will simply return the response in a payload property as parsed
     * JSON data.
     *
     * @param { fs.PathLike } host The remote host (i.e. https://gitlab.com )
     * @param { String } query The query portion of the url (or path with query string i.e. /api/v4/projects/802598565/repository/tree?recursive=true)
     * @param { fs.PathLike | boolean = false } destination The destination path to write the response to (defaults to boolean false, indicating to
     *                                                      return the response in a payload property)
     *
     * @returns { ClientHttp2Promise }
     */
    public static async makeRequest<T extends ClientHttp2Promise>( host: fs.PathLike, query: String,  destination: fs.PathLike | boolean = false ): Promise<T> {
        return new Promise(
            ( resolve, reject ) => {
                // Connect to the host:
                DEBUG( `Connect to client '${host}'` );
                const client = http2.connect( host as any );
                client.on( 'error', ( error ) => reject( error ) );

                // Create [make] a request:
                DEBUG( `Send request '${query}' to host '${host}'` );
                const request = client.request( {  ':path': query as any } );

                // Handle the response:
                request.on(
                    'response',
                    async ( headers, flags ) => {
                        DEBUG( `Response from server '${headers[':status']}'` );

                        // If not success (!==200), handle that:
                        if( headers[':status'] !== 200 )
                            reject( new Error( `Server responded with ${headers[':status']}` ) );

                        if( destination ) {
                            // Write to file or folder
                            DEBUG( 'Write resource to disk' );

                            resolve( await this.writeToDisk( client, request, destination as fs.PathLike ) );
                        }
                        else {
                            DEBUG( `Receive '${headers['content-length']}' bytes from stream:` );

                            resolve( await this.readFromStream( client, request ) as T );
                        }
                    }
                );

                // Fire off [flush] the request:
                request.end();
            }
        );
    }


    /**
     * Method to write a readable ClientHttp2Stream to disk
     *
     * @param { http2.ClientHttp2Session } client The Http2 session
     * @param { http2.ClientHttp2Stream } request The Http2 readable stream
     * @param { fs.PathLike } destination The destination path to write the readable stream data to
     *
     * @returns { Promise<ClientHttp2Promise> }
     */
    public static async writeToDisk<T extends ClientHttp2Promise>( client: http2.ClientHttp2Session, request: http2.ClientHttp2Stream, destination: fs.PathLike ): Promise<T> {

        return new Promise(
            ( resolve, reject ) => {

                // Create a fileWriteStream:
                DEBUG( `Create 'fileWriteStream'` );
                const fullPath = _path.join( Filesystem.getPathToCWD(), destination as any );
                const file = fs.createWriteStream( fullPath, { flags: "wx" } );

                // Write data chunks to file:
                DEBUG( `Set 'http2.ClientHttp2Stream' encoding to 'utf8'` );
                request.setEncoding( 'utf8' );

                // Write data chunks to file:( fullPath, { flags: "wx" } );
                DEBUG( `Pipe 'http2.ClientHttp2Stream' to 'fileWriteStream'` );
                request.pipe( file );

                file.on(
                    'finish',
                    () => {
                        DEBUG( `End of request` );
                        file.close();
                        client.close();
                        resolve( { fileSystemPromiseBits: { result: true, path: destination, fullPath } } as T );
                    }
                )

                file.on( 'error', error => fs.unlink( fullPath, () => reject( error ) ) );
            }
        );
    }


    /**
     * Method to read the data from a ClientHttp2Stream to a JavaScript object
     *
     * @param { http2.ClientHttp2Session } client The Http2 session
     * @param { http2.ClientHttp2Stream } request The Http2 readable stream
     *
     * @returns { Promise<ClientHttp2Promise> }
     */
    public static async readFromStream<T extends ClientHttp2Promise>( client: http2.ClientHttp2Session, request: http2.ClientHttp2Stream ): Promise<T> {
        return new Promise(
            ( resolve, reject ) => {
                let payload = '';

                // Write data chunks to var:
                request.setEncoding( 'utf8' );
                request.on( 'data', chunk => { DEBUG( `Read '${chunk.length}' byte(s) from stream.` ); payload += chunk; } );

                // Handle ending the request
                request.on(
                    'end',
                    () => {
                        DEBUG( `End request: '${payload.length}' byte(s) received` );
                        client.close();
                        resolve( { fileSystemPromiseBits: { result: true }, payload: JSON.parse( payload ) } as T );
                    }
                );
            }
        );
    }

}