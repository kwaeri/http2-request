# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-http2-request [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The kwaeri/http2-request component for the kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/http2-request/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/http2-request/commits/main)  [![coverage report](https://gitlab.com/kwaeri/http2-request/badges/main/coverage.svg)](https://kwaeri.gitlab.io/http2-request/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The kwaeri/http2-request component module was designed to aid [kwaeri CLI](https://gitlab.com/kwaeri/user-executable) service providers make requests over the internet using Node's built-in HTTP2 module. It's a simple tool that simplifies the process of using HTTP2, and the latest release presents a pure ESM implementation that built with async/await.

## Getting Started

**NOTE**

kwaeri/http2-request is not ready for production, and has only been published for testing and development purposes. You're free to try out anything that may already be available, but please be aware that there is likely to be many aspects of the software which are not [fully] working and/or are completely broken. As the new platform nears completion, updated documentation and complete tutorials and examples will be provided.

### Installation

[kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps a myriad of node-kit components under the kwaeri scope specific to the MV(a)C application framework, and provides a single entry point to the framework for easing the process of building a kwaeri application.

[kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the myriad of components under the kwaeri scope specific to the user executable framework, and provides a single entry point to the user executable (CLI).

To install this module, run - from terminal:

```bash
npm install @kwaeri/http2-request
```

### Usage

To leverage the http2-request component module, you'll first need to include it:

```typescript
// INCLUDES
import { Http2Request, ClientHttp2Promise } from '@kwaeri/http2-request';

//...
```

Next, call the static method `makeRequest`, passing the host and query string as parameters to the invocation:

```typescript
const host = 'https://gitlab.com',
      query = 'api/v4/projects/37335286/repository/commits/main';

const { payload } = await Http2Request.makeRequest( host, query );

// ...
```

The payload will contain the result of the request.

Alternatively, you can redirect the payload to a file by supplying a path as a third argument:

```typescript
const host = 'https://gitlab.com',
      query = 'api/v4/projects/37335286/repository/commits/main',
      file = 'payload.json';

const { fileSystemPromiseBits }  = await Http2Request.makeRequest( host, query, path );

const {
  result,
  path,
  fullPath
} = ( await Http2Reqeust.makeRequest( host, query, file ) ).fileSystemPromiseBits;

console.log( `Saved request response as '${path}' at '${fullPath}' successfully: '${result}'` );

// ...
```

To be continued...

**NOTE**

As mentioned earlier, the plan is to continue development of the myriad components of the node-kit platform - the http2-request component included - and ultimately ease the process of development, maintainence, and usage of each individual component as they are decoupled from one another.


## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/groups/kwaeri/node-kit/-/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [Service Desk](mailto:incoming+kwaeri-http2-request-25179063-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/groups/kwaeri/node-kit/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
