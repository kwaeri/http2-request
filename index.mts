/**
 * SPDX-PackageName: kwaeri/http2-request
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES


// ESM WRAPPER
export type {
    ClientHttp2Promise,
} from './src/http2-request.mjs';

export {
    Payload,
    Http2Request
} from './src/http2-request.mjs'
